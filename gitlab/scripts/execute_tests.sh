#!/usr/bin/env bash

python -V
export PWD=$(pwd)
echo $PWD
echo $MODULE_PATH
cd $MODULE_PATH
export PYVER=$(cat ./.python-version)
echo $PYVER
echo $CI_DEPLOY_USER
pip install pipenv
pipenv sync -d
curl -fsSL "http://elastic:elastic@elasticsearch:9200/_cat/health?h=status"
pipenv run coverage run -m pytest
pipenv run coverage report
