{
  trigger(parent, name, path):: {
    stage: 'initialize',
    variables: {
      MODULE_PATH: path
    },
    trigger: {
      include: 'service.gitlab-ci.yml',
    },
    rules: [
      {
//        'if': '$COMMIT_REF_NAME == $DEFAULT_BRANCH',
        changes: [path + '/**/*'],
      },
    ],
  },
  build(name, path, workspace):: {
    stage: "build",
    script: [
        "echo 'This job builds " + name + " image.'"
    ],
    rules: [
      {
//        'if': '$COMMIT_REF_NAME == $DEFAULT_BRANCH',
        changes: [path + '/**/*'],
      },
    ],
  },
  test(name, path, workspace):: {
    stage: "test",
    needs: [
      {
        job:"build-"+workspace,
        optional: true
      }
    ],
    rules: [
      {
//        'if': '$COMMIT_REF_NAME == $DEFAULT_BRANCH',
        changes: [path + '/**/*'],
      },
    ],
    when: "on_success",
    variables: {
        FF_NETWORK_PER_BUILD: "true",
        ELASTIC_PASSWORD: "elastic",
        MODULE_PATH: path
    },
    script: [
        "echo 'This job tests " + name + " service.'",
        "echo 'This job tests ${MODULE_PATH}"
    ]
  },
  deploy(name, path, workspace):: {
    stage: "deploy",
    needs: [
      {
        job:"test-"+workspace,
        optional: true
      }
    ],
    rules: [
      {
//        'if': '$COMMIT_REF_NAME == $DEFAULT_BRANCH',
        changes: [path + '/**/*'],
      },
    ],
    when: "on_success",
    script: [
        "echo 'This job deploys " + name + " service.'"
    ]
  },
}
