local jobs = import 'jobs.jsonnet';

local services = [
    'alpha',
    'beta'
];

local str_join(parent, service, delimiter='/') = parent + delimiter + service;
local service_config(service, parent='services') = {
    name: service,
    path: str_join(parent, self.name),
    workspace: str_join(parent, self.name, '-')
};

local conf = {
  services: [
    service_config(service) for service in services
  ],
};

//{
//  stages: [ stage for stage in stages ],
//} + {
//  // Building plan jobs
//  ['build-' + svc_config.workspace]: jobs.build(svc_config.name, svc_config.path, svc_config.workspace)
//  for svc_config in conf.services
//} + {
//  // Building plan jobs
//  ['test-' + svc_config.workspace]: jobs.test(svc_config.name, svc_config.path, svc_config.workspace)
//  for svc_config in conf.services
//} + {
//  // Building plan jobs
//  ['deploy-' + svc_config.workspace]: jobs.deploy(svc_config.name, svc_config.path, svc_config.workspace)
//  for svc_config in conf.services
//}

{
  // Building plan jobs
  ['trigger:' + svc_config.workspace]: jobs.trigger('services', svc_config.name, svc_config.path)
  for svc_config in conf.services
}